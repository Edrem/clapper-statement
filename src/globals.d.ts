declare module "*.scss" {
    const value: {
        [index: string]: string;
    }
    export default value;
}
import { h, render, Component } from "preact";
import style from "./index.scss";

const clapper = "👏";

interface IClapperState {
    value: string;
}

class Clapper extends Component<{}, IClapperState> {
    constructor(props: {}) {
        super(props);

        this.state = { value: "" };
        this.onChangeValue = this.onChangeValue.bind(this);
    }

    private onChangeValue(e: any) {
        console.log("asd");
        this.setState({ value: e.currentTarget.value });
    }

    public render(): JSX.Element {
        return (
            <div>
                <div>
                    <textarea placeholder="Write something" onInput={this.onChangeValue} value={this.state.value} />
                </div>
                <div>
                    <textarea readOnly value={this.state.value.replace(/ /g, clapper) + clapper} />
                </div>
            </div>
        );
    }
}

render(
    <div className={style.container}>
        <h1>Clapper Statement</h1>
        <p>Adds clapping hands to what you say</p>
        <Clapper />
        <footer>
            Made out of sheer boredom  | Joshua Bax
        </footer>
    </div>,
    document.getElementById("container") as HTMLElement,
);
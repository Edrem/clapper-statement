const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
    output: {
        path: path.join(__dirname, 'dist'),
        filename: "[name].js",
        chunkFilename: "[name]-[id].js",
    },
    entry: {
        bundle: [
            './src/index.tsx',
        ],
    },
    module: {
        rules: [{
                test: /\.scss|.css$/,
                use: ["style-loader", "css-loader", "sass-loader"],
            },
            {
                test: /\.tsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                    silent: true,
                },
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
        modules: [
            "./node_modules",
        ],
        plugins: [new TsconfigPathsPlugin({
            configFile: "./tsconfig.json"
        })]
    },

    plugins: [
        new HTMLWebpackPlugin({
            chunks: ["bundle"],
            template: "./src/index.html",
        })
    ]
};
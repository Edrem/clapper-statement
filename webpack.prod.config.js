const base = require("./webpack.base.config");
const merge = require("webpack-merge");
const TerserPlugin = require("terser-webpack-plugin");
const path = require("path");

module.exports = merge.smart(base, {
    mode: "production",
    output: {
        path: path.join(__dirname, 'public'),
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: true,
            }),
        ]
    },
});